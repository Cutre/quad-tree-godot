using Godot;
using System;
using System.Collections.Generic;

namespace Quadtree
{   
    /// <summary>
    /// Demo class
    /// </summary>
    public class QuadController : Node2D
    {
        /// <summary>
        /// Root QuadTree
        /// </summary>
        QuadTree<Vector2> t;

        /// <summary>
        /// Quads side color
        /// </summary>
        [Export]
        Color sideColor;

        /// <summary>
        /// Color of the points
        /// </summary>
        [Export]
        Color pointColor;

        /// <summary>
        /// Number of points
        /// </summary>
        [Export]
        int numberPoint;
        
        [Export]
        /// <summary>
        /// Field width
        /// </summary>
        int width;

        [Export]
        /// <summary>
        /// Field height
        /// </summary>
        int heigh;

        /// <summary>
        /// Sort options.If manual, have to press "space" to insert a new random point
        /// </summary>        
        enum Sort : byte{AUTOMATIC, MANUAL}
        [Export]
        Sort MySort;

        int index;
        
        public override void _Ready()
        {
            this.Init();        
        }

        /// <summary>
        /// Init func
        /// </summary>
        private void Init(){
             t = new QuadTree<Vector2>(new Vector2(0,0), new Vector2(this.width,this.heigh));           
            
            if(this.MySort == Sort.AUTOMATIC){
                this.AddPoints();  
            }          
            this.index = 0;  
        }

        /// <summary>
        /// Draws the quads and the points
        /// </summary>
        public override void _Draw(){
            GD.Print("Draw");
            
            //this.Createquad(t.TopLeft, t.BottomRight, this.sideColor);
            this.DrawQuad();            
        }

        /// <summary>
        /// Creates a quad
        /// <para>TODO: Use DrawRectangle, it have a not filled option 
        /// </summary>
        /// <param name="topleft">Top left corner of the quad</param>
        /// <param name="bottomRight">Top right corner of the quad</param>
        /// <param name="col">Color of the border</param>
        /// <param name="thick">Thickness</param>
        private void Createquad(in Vector2 topleft, in Vector2 bottomRight, in Color col, in float thick = 2){
            base.DrawLine(topleft, new Vector2(bottomRight.x, topleft.y), col, thick);
            base.DrawLine(new Vector2(bottomRight.x, topleft.y), bottomRight, col, thick);
            base.DrawLine(bottomRight, new Vector2(topleft.x, bottomRight.y), col, thick);
            base.DrawLine(new Vector2(topleft.x, bottomRight.y), topleft, col, thick);
        }
        
        /// <summary>
        /// Adds as many as /// <see cref="numberPoint"/> points to the quadtree
        /// </summary>
        private void AddPoints(){
            int randomX;
            int randomY;

            Random rand = new Random();

            bool isInserted = false;
            for(int i = 0; i < numberPoint; i++){
                randomX = rand.Next(0, this.width);
                randomY = rand.Next(0, this.heigh);

                Vector2 pos = new Vector2(randomX, randomY);                             
                //GD.Print("Pos: " + pos);
                isInserted = t.Insert(in pos, pos);
                //GD.Print("Node " + i + " inserted: " + isInserted);                           
            }
        }
        
        #region Draw
        private void DrawQuad(){
            this.DrawQuadTree(this.t);
        }

        
        private void DrawQuadTree(in QuadTree<Vector2> quad){

            this.Createquad(quad.TopLeft, quad.BottomRight, this.sideColor);

            QuadTree<Vector2> temp;

            for(byte i = 0; i < quad.Children; i++){
                temp = quad[i];

                if(temp == null){
                    this.DrawPoints(quad);
                    return;
                }

                //colocamos el nuevo como busqueda
                this.DrawQuadTree(temp);
            }
        }
        

        private void DrawPoints(in QuadTree<Vector2> quad){
            QNode<Vector2> tempNode;

            for(byte i = 0; i < quad.MyNodes.Length; i++){
                
                if(quad.MyNodes[i].HasValue){
                    tempNode = (QNode<Vector2>) quad.MyNodes[i];
                    base.DrawCircle(tempNode.Position, 4, this.pointColor);
                }
                
            }
        }
        #endregion
        
        private void AddOnlyOnePoint(){

            if(this.numberPoint <= this.index){
                return;
            }

            index++;

            int randomX;
            int randomY;

            Random rand = new Random();

            randomX = rand.Next(0, this.width);
            randomY = rand.Next(0, this.heigh);

            Vector2 pos = new Vector2(randomX, randomY);                             
            GD.Print("Pos: " + pos);
            bool isInserted = t.Insert(in pos, pos);
            GD.Print("Node " + index + " inserted: " + isInserted);
            base.Update();    

        }
        

        public override void _Process(float delta){
            
            if(Input.IsActionJustPressed("ui_select")){
                
                if(this.MySort == Sort.MANUAL){
                    this.AddOnlyOnePoint();
                    GD.Print("Pressed");
                }
                else{
                    //Reset the points and the quadtree
                    this.Init();
                    base.Update();
                }                
            }         
        }
    }
}