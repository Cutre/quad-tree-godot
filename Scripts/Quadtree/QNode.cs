using Godot;
using System;

namespace Quadtree{
    /// <summary>
    /// The node struct that we put at the Quadtree
    /// </summary>
    public struct QNode<T>
    {
        /// <summary>
        /// Data class. We can put wathever
        /// </summary>    
        public T MyData{get;set;}
        
        /// <summary>
        /// Position of the node.
        /// </summary>
        public Vector2 Position{get;set;}
    
        public QNode(T myObject, Vector2 pos){
            this.MyData = myObject;
            this.Position = pos;
        }            

    }
}