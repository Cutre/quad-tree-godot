using Godot;
using System;

namespace Quadtree{

    public class QuadTree<T>
    {        
        /// <summary>
        /// TopLeft point of the box
        /// </summary>        
        public Vector2 TopLeft{get;set;}

        /// <summary>
        /// Bottom Right point of the box
        /// </summary>
        public Vector2 BottomRight{get;set;}

         /// <summary>
        /// Array of nodes
        /// </summary> 
        public QNode<T>?[] MyNodes{get;set;}                 
        
        #region QuadTree<T> childs
        public QuadTree<T> TopLeftTree{get;set;}
        public QuadTree<T> TopRigthTree{get;set;}
        public QuadTree<T> BottomLeftTree{get;set;}
        public QuadTree<T> BottomRigthTree{get;set;}
        #endregion

        #region Extended Properties
        /// <summary>
        /// The number of Quadtree children
        /// </summary>
        public byte Children{
            get{return 4;}
        }

        /// <summary>
        /// Capacity of the node array
        /// </summary>
        public int Capacity{
            get {return 4;}
        }
        #endregion

        #region Constructors
        public QuadTree(){
            this.TopLeft = Vector2.Zero;
            this.BottomRight = Vector2.Zero;       
        }

        public QuadTree(Vector2 topLeft, Vector2 bottomRigth){
            this.TopLeft = topLeft;
            this.BottomRight = bottomRigth;
            this.MyNodes = new QNode<T>?[this.Capacity];

        }
        #endregion
       
        #region Insert Boundary Funcs
        /// <summary>
        /// Inserts a <see cref="QNode<T>"/> with the data in the correct QuadTree
        /// </summary>
        /// <param name="pos">The position of the object</param>
        /// <param name="obj">The data object</param>
        /// <returns>Is the node inserted?</returns>
        public bool Insert(in Vector2 pos, in T obj){

            if(this.Boundary(pos) == false){                              
                return false;
            }

            int index;

            if(this.TopLeftTree == null){
                if(this.CheckArray(out index)){                    
                    this.MyNodes[index] = new QNode<T>(obj, pos);
                    return true;            
                }                
                this.Subdivide(this);
            }

            if(this.TopLeftTree.Insert(pos, obj)){                              
                return true;
            }            
            else if(this.TopRigthTree.Insert(pos, obj)){                
                return true;
            }            
            else if(this.BottomRigthTree.Insert(pos, obj)){                
                return true;
            }           
            else if(this.BottomLeftTree.Insert(pos, obj)){                
                return true;
            }

            GD.Print("Error 2");
            GD.Print("Pos " + pos);
            GD.Print("Array" + this.MyNodes);
            int inxed;
            GD.Print("CheckArray" + this.CheckArray(out inxed));
            GD.Print("Array Index: " + inxed);
            GD.Print("TopLeft " + this.TopLeftTree);
            GD.Print("TopRight " + this.TopRigthTree);
            GD.Print("BottomRight " + this.BottomRigthTree);
            GD.Print("BottomLeft" + this.BottomLeftTree);
            return false; 
        }

        /// <summary>
        /// Checks if a point is inside a rectangle.
        /// </summary>
        /// <param name="pos">The point pos</param>
        private bool Boundary(in Vector2 pos){
            return(this.TopLeft.x <= pos.x &&
                   this.TopLeft.y <= pos.y &&
                   this.BottomRight.x >= pos.x &&
                   this.BottomRight.y >= pos.y);
        }
        #endregion

        #region Check Array
        /// <summary>
        /// Checks for an empy spot in the array.
        /// <para>TODO: We are gona put this in a custom array class. 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool CheckArray(out int index){

            for (int i = 0; i < this.MyNodes.Length; i++)
            {
                if(this.MyNodes[i] == null){
                                     
                    index = i;
                    return true;
                }
            }
            index = -1;
            return false;
        }
        #endregion

        #region Subvidide funcs
        /// <summary>
        /// Subdivides a <see cref="QuadTree<T>" in four zones
        /// </summary>
        /// <param name="quad">The <see cref="QuadTree<T>" to divide</param>
        private void Subdivide(QuadTree<T> quad){
            Vector2 tempVector = new Vector2((quad.BottomRight.x - quad.TopLeft.x )/2, 
                                             (quad.BottomRight.y - quad.TopLeft.y)/2);  

            float middleX = quad.TopLeft.x + tempVector.x;
            
            // OJU!!!! Downing the Y it's positive!            
            float middleY = quad.TopLeft.y + tempVector.y;

            //GD.Print("This quad: TL:" + this.TopLeft + "//BR:" + this.BottomRight);

            quad.TopLeftTree = new QuadTree<T>(quad.TopLeft, new Vector2(middleX,middleY));  

            quad.BottomLeftTree = new QuadTree<T>(new Vector2(quad.TopLeft.x, middleY), 
                                                  new Vector2(middleX, quad.BottomRight.y));                                             
            
            quad.TopRigthTree = new QuadTree<T>(new Vector2(middleX, quad.TopLeft.y), 
                                            new Vector2(quad.BottomRight.x, middleY));

            quad.BottomRigthTree = new QuadTree<T>(new Vector2(middleX,middleY), quad.BottomRight);
            
            //Tenemos que meter los puntos en este lado :S
            this.ArrayToNewNodes();          
        }        

        /// <summary>
        /// Passes the data of this <see cref="MyNodes"/> to the new build Quadtrees.
        ///<para>Used by default, is utilized to have only the data to the leaves.  
        /// </summary>
        private void ArrayToNewNodes(){
            QNode<T> tempNode;

            for (byte i = 0; i < this.Capacity; i++)
            {   
                if(this.MyNodes[i].HasValue){
                    tempNode = (QNode<T>) this.MyNodes[i];
                }
                else{
                    //TODO:
                    //We will make a class with an ordered array so, if the Array[0] is empty, means
                    //that the othear are empty aswell.
                    //(A list is better?)
                    continue;
                }

                if(this.Insert(tempNode.Position, tempNode.MyData)){
                    this.MyNodes[i] = null;
                }                                
            }
        }
        #endregion

        #region Indexes
        /// <summary>
        /// Returns a child Quadtree based on an index.
        /// <para>
        /// 0 = TopLeftTree<para>
        /// 1 = TopRightTree<para>
        /// 2 = BottomRightTree<para>
        /// 3 = BottomLeftTree
        /// default = null;
        /// </summary>
        public QuadTree<T> this[int index]{
            get{ return SetIndexes(index);}
        }

        /// <summary>
        /// Return a Quatree child based on an index
        /// </summary>
        /// <param name="index">Index of the quadTree desired</param>
        /// <returns></returns>
        private QuadTree<T> SetIndexes(in int index){
            switch(index){
                case 0:
                    return this.TopLeftTree;                    
                case 1:
                    return this.TopRigthTree;
                case 2:
                    return this.BottomRigthTree;
                case 3:
                    return this.BottomLeftTree;
                default:
                    return null;
            }
        }
        #endregion
     
    }
}