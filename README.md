This is a personal practice on making a quadtree for detecting collisions.
Now only allows to add nodes and there's no collision detection but it's no really hard to implementate.
There's room to optimize the code, change little things and maybe replace some parts but I think it's a great point to start.

OJU!
Uses C# 7.2 so, for previous versions please remove the "in" keyword in some functions parameters.
About the "Godot" namespace, except for the controller, it only uses "Vector2" struct.
